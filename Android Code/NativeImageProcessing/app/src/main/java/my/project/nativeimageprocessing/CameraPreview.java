package my.project.nativeimageprocessing;

/**
 * Created by ducdang on 10/24/16.
 */

import android.app.Activity;
import android.graphics.Paint;
import android.hardware.Camera;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.AdvancedLineAndPointRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.XYPlot;
import java.io.IOException;
import java.util.Arrays;
import static android.hardware.Camera.Parameters.FOCUS_MODE_FIXED;
import static android.hardware.Camera.Parameters.FOCUS_MODE_MACRO;


@SuppressWarnings("deprecation")
public class CameraPreview implements SurfaceHolder.Callback,
        Camera.PreviewCallback, ImageProcessingThread.Callback {
    private Camera mCamera = null;
    private int PreviewSizeWidth;
    private int PreviewSizeHeight;
    private static final String TAG = "MyCamera";
    private int count=0;
    public boolean isStopped = false;
    private ImageProcessingThread mWorkerThread;
    private PPGSeries ppgSeries;
    private XYPlot plot;
    private Redrawer redrawer;

    public CameraPreview(Activity activity, int PreviewlayoutWidth, int PreviewlayoutHeight)
    {
        PreviewSizeWidth = PreviewlayoutWidth;
        PreviewSizeHeight = PreviewlayoutHeight;
        if (plot == null){
            initPlot(activity);
        }
        //
        redrawer = new Redrawer(plot, 30, true);
    }

    private void initPlot(Activity activity){
        plot = (XYPlot) activity.findViewById(R.id.plot);
        int displayDuration = 3; // display duration in second(s)
        ppgSeries = new PPGSeries(displayDuration*30);
        //XYSeries s1 = new SimpleXYSeries(SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "PPG signal", 1,5,2,8,3,9);
        //plot.addSeries(s1, new LineAndPointFormatter(Color.GREEN,Color.GREEN,null,null));
        plot.addSeries(ppgSeries, new MyFadeFormatter(displayDuration*30));
        plot.setRangeBoundaries(0, 255, BoundaryMode.AUTO);
        plot.setDomainBoundaries(0, displayDuration*30, BoundaryMode.FIXED);

        // reduce the number of range labels
        plot.setLinesPerRangeLabel(3);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera mCamera)
    {
        if (!isStopped){
            //count = count + 1;
            mWorkerThread.queueTask(count, PreviewSizeWidth, PreviewSizeHeight, data);
            Log.d(TAG, "Current time: "  + System.currentTimeMillis());
            //Log.d(TAG, "Current time: "  + mCamera.getParameters().flatten());
            //Log.d(TAG, "Current time: "  + mCamera.getParameters().get("iso-values"));
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3)
    {
        mWorkerThread = new ImageProcessingThread(new Handler(), this);
        mWorkerThread.start();
        mWorkerThread.prepareHandler();

        Camera.Parameters parameters;
        parameters = mCamera.getParameters();

        String flat = parameters.flatten();

        parameters.setPreviewSize(PreviewSizeWidth, PreviewSizeHeight);
        parameters.setPictureSize(PreviewSizeWidth, PreviewSizeHeight);

        parameters.setPreviewFrameRate(30);
        parameters.setPreviewFpsRange(30000, 30000);

        parameters.setExposureCompensation(-5);
        //parameters.setAutoExposureLock(true);
        //parameters.setWhiteBalance("fluorescent");
        //parameters.set("iso", "ISO800");

        String NowFlashMode = parameters.getFlashMode();
        if ( NowFlashMode != null )
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

        String NowFocusMode = parameters.getFocusMode ();

        String[] focusModes = null;
        String focus_keyword=null;
        //String focus_modes_keyword=null;
        if ( NowFocusMode != null ) {
            if(flat.contains("focus-mode-values")){
                focus_keyword="focus-mode-values";
                //focus_modes_keyword="focus-mode";
                String focus_modes = flat.substring(flat.indexOf(focus_keyword));
                focus_modes = focus_modes.substring(focus_modes.indexOf("=")+1);

                if(focus_modes.contains(";"))
                        focus_modes = focus_modes.substring(0, focus_modes.indexOf(";"));

                focusModes = focus_modes.split(",");

                if (Arrays.asList(focusModes).contains("macro")){
                    parameters.setFocusMode(FOCUS_MODE_MACRO);
                    Log.i(TAG, "Focus: "  + parameters.getFocusMode());
                    //Log.i(TAG, "ISO: "  + parameters.get("iso"));
                }else{
                    parameters.setFocusMode(FOCUS_MODE_FIXED);
                    Log.i(TAG, "Focus: "  + parameters.getFocusMode());
                    //Log.i(TAG, "ISO: "  + parameters.get("iso"));
                }
            }
    }
        //Log.i(TAG, "Focus Distance: "  + parameters.getFocusDistances());
        mCamera.setParameters(parameters);
        mCamera.startPreview();

    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0)
    {
        mCamera = Camera.open();
        try
        {
            mCamera.setPreviewDisplay(arg0);
            mCamera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0)
    {
        stopWorkerThread();
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
        //redrawer.finish();
    }

    public void stopWorkerThread(){
        if(mWorkerThread != null){
            if (mWorkerThread.isAlive())
                mWorkerThread.quitSafely();
        }
    }

    public void stopPreview(){
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }

    }
    @Override
    public void onNewValue(double newValue){
        if (mCamera != null) {
            Log.i(TAG, "Exposure: " + mCamera.getParameters().getExposureCompensation());
            //Log.i(TAG, "ISO: " + mCamera.getParameters().get("iso"));
        }
        if(ppgSeries != null) {
            ppgSeries.addSample(newValue);
            ((AdvancedLineAndPointRenderer) plot.
                    getRenderer(AdvancedLineAndPointRenderer.class)).setLatestIndex(ppgSeries.getLatestIndex());
        }
    }

    /**
     * Special {@link AdvancedLineAndPointRenderer.Formatter} that draws a line
     * that fades over time.  Designed to be used in conjunction with a circular buffer model.
     */
    public static class MyFadeFormatter extends AdvancedLineAndPointRenderer.Formatter {
        private int trailSize;
        public MyFadeFormatter(int trailSize) {
            this.trailSize = trailSize;
        }
        @Override
        public Paint getLinePaint(int thisIndex, int latestIndex, int seriesSize) {
            // offset from the latest index:
            int offset;
            if(thisIndex > latestIndex) {
                offset = latestIndex + (seriesSize - thisIndex);
            } else {
                offset =  latestIndex - thisIndex;
            }

            float scale = 255f / trailSize;
            int alpha = (int) (255 - (offset * scale));
            getLinePaint().setAlpha(alpha > 0 ? alpha : 0);
            return getLinePaint();
        }
    }
}