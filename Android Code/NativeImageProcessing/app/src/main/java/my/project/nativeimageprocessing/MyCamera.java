package my.project.nativeimageprocessing;

/**
 * Created by ducdang on 10/24/16.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import org.opencv.android.OpenCVLoader;

@SuppressWarnings("deprecation")
public class MyCamera extends Activity {
    private CameraPreview camPreview;
    private FrameLayout mainLayout;
    private int PreviewSizeWidth = 640;
    private int PreviewSizeHeight= 480;
    private Button buttonClick;
    private SurfaceView camView;
    private SurfaceHolder camHolder;
    private static final String TAG = "MyCamera";
    private Handler stoppingHandler = new Handler();

    static {
        System.loadLibrary("imageProcessing");
        if (!OpenCVLoader.initDebug()){
            Log.d(TAG,"OpenCV loaded");
        }else{
            Log.d(TAG,"OpenCV not loaded");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        // The request code used in ActivityCompat.requestPermissions()
        // and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        showCameraPreview();
    }

    private void showCameraPreview(){
        camView = new SurfaceView(this);
        camHolder = camView.getHolder();
        camPreview = new CameraPreview(this, PreviewSizeWidth, PreviewSizeHeight);
        camHolder.addCallback(camPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mainLayout = (FrameLayout) findViewById(R.id.frameLayout1);
        mainLayout.addView(camView, new ViewGroup.LayoutParams((int)(PreviewSizeWidth), (int)(PreviewSizeHeight)));
        stoppingHandler.postDelayed(stopCollectingData, 120000);

        buttonClick = (Button) findViewById(R.id.button);
        buttonClick.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                if (!camPreview.isStopped){
                    camPreview.isStopped = true;
                    camView.getHolder().removeCallback(camPreview);
                    camPreview.stopPreview();
                    camPreview.stopWorkerThread();
                }
            }
        });

        buttonClick = (Button) findViewById(R.id.button_restart);
        buttonClick.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                recreateActivityCompat(MyCamera.this);
            }
        });
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onPause(){
        super.onPause();
        if (!camPreview.isStopped){
            camPreview.isStopped = true;
            camView.getHolder().removeCallback(camPreview);
            camPreview.stopPreview();
            camPreview.stopWorkerThread();
        }
    }

    private Runnable stopCollectingData = new Runnable() {
        @Override
        public void run() {
            if (!camPreview.isStopped){
                camPreview.isStopped = true;
                camView.getHolder().removeCallback(camPreview);
                camPreview.stopPreview();
                camPreview.stopWorkerThread();

            }
        }
    };

    /**
     Current Activity instance will go through its lifecycle to onDestroy() and a new instance then created after it.
     */
    @SuppressLint("NewApi")
    public static final void recreateActivityCompat(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            activity.recreate();
        } else {
            final Intent intent = activity.getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            activity.finish();
            activity.overridePendingTransition(0, 0);
            activity.startActivity(intent);
            activity.overridePendingTransition(0, 0);
        }
    }
}


