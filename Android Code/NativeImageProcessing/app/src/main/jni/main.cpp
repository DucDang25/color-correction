//
// Created by ducdang on 12/3/16.
//

#include <jni.h>
#include "my_project_nativeimageprocessing_ImageProcessingThread.h"
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;
extern "C" {
    JNIEXPORT jdouble JNICALL Java_my_project_nativeimageprocessing_ImageProcessingThread_nativeCorrectingImage(JNIEnv *env, jobject obj,
                                                                                                    jlong matAddrProg, jlong matAddrConverting) {

        Mat& convertingMat   = *(Mat*)matAddrConverting;
        Mat& processedImgYUV = *(Mat*)matAddrProg;
        Mat  processedImgRGB;
        cvtColor(processedImgYUV, processedImgRGB, CV_YUV420sp2RGB, 3);

        vector<Mat> planes;
        split(processedImgRGB,planes);

        Mat redTransposed = planes[0].t();
        Mat redColumn = redTransposed.reshape(1,1).t();
        Mat greenTransposed = planes[1].t();
        Mat greenColumn = greenTransposed.reshape(1,1).t();
        Mat blueTransposed = planes[2].t();
        Mat blueColumn = blueTransposed.reshape(1,1).t();
        processedImgRGB = Mat::ones(480*640, 1, redColumn.type());
        hconcat(processedImgRGB, redColumn, processedImgRGB);
        hconcat(processedImgRGB, greenColumn, processedImgRGB);
        hconcat(processedImgRGB, blueColumn, processedImgRGB);
        processedImgRGB.convertTo(processedImgRGB,CV_32FC1);

        Mat matReshapedCorrected = processedImgRGB * convertingMat;
        cv:Scalar tempAvg = mean(matReshapedCorrected.col(1));
        jdouble greenAvg = tempAvg[0];

        processedImgYUV = cv::Mat();
        processedImgRGB = cv::Mat();
        redTransposed = cv::Mat();
        redColumn = cv::Mat();
        greenTransposed = cv::Mat();
        greenColumn = cv::Mat();
        blueTransposed = cv::Mat();
        blueColumn = cv::Mat();

        return greenAvg;
    }
}